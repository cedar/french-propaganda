import json
import os
import random
from pathlib import Path

import evaluate
import numpy as np
import torch
from datasets import load_dataset
from sklearn.utils.class_weight import compute_class_weight
from torch import nn
from transformers import (
    AutoModelForSequenceClassification,
    AutoTokenizer,
    DataCollatorWithPadding,
    Trainer,
    TrainingArguments,
)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


PARAMS = {
    "model_name": "bert-base-multilingual-cased",
    "learning_rate": 2e-5,
    "batch_size_train": 16,
    "batch_size_eval": 64,
    "save_model": True,
    "epoch": 10,
    "logging_step": 50,
    "save_step": 50,
    "eval_step": 50,
    "seed": 42,
    "warmup_ratio": 0.1,
    "weight_decay": 0.01,
    "clean_results": True,
    "do_eval": True,
    "class_weight": None,
}

BASE_DIR = Path(__file__).parent.parent
DATA_DIR = BASE_DIR.joinpath("data")
RES_DIR = BASE_DIR.joinpath("results")


def set_seed(seed):
    """
    Everything should be reproducible
    :param seed:
    :return:
    """
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    random.seed(seed)
    np.random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    # these are just for deterministic behaviour
    torch.backends.cudnn.benchmark = False
    torch.use_deterministic_algorithms(True)
    os.environ["CUBLAS_WORKSPACE_CONFIG"] = ":4096:8"
    torch.backends.cudnn.deterministic = True


def clean_dir(path):
    for child in path.glob("*"):
        if child.is_file():
            child.unlink()
        else:
            clean_dir(child)
    path.rmdir()


def preprocess_function(tokenizer):
    return lambda example: tokenizer(example["sentence"], truncation=True)


def compute_metrics(eval_preds):
    metric = evaluate.load("f1", average="macro")
    logits, labels = eval_preds
    predictions = np.argmax(logits, axis=-1)
    return metric.compute(predictions=predictions, references=labels)


class CustomTrainer(Trainer):
    def compute_loss(
        self,
        model,
        inputs,
        return_outputs=False,
    ):
        labels = inputs.get("labels")
        outputs = model(**inputs)
        logits = outputs.get("logits")
        loss_fct = nn.CrossEntropyLoss(
            weight=torch.tensor(PARAMS["class_weight"], dtype=torch.float).to(device)
        )
        loss = loss_fct(logits.view(-1, self.model.config.num_labels), labels.view(-1))
        return (loss, outputs) if return_outputs else loss


if __name__ == "__main__":

    MODEL_DIR = BASE_DIR.joinpath("models", PARAMS["model_name"])
    if not MODEL_DIR.exists():
        MODEL_DIR.mkdir()
    RUN_DIR = MODEL_DIR.joinpath(str(len(list(MODEL_DIR.iterdir()))))

    print("Starting training with PARAMS:\n", PARAMS, "\n")

    set_seed(PARAMS["seed"])

    model_name = PARAMS["model_name"]
    id2label = {0: "NEGATIVE", 1: "POSITIVE"}
    label2id = {"NEGATIVE": 0, "POSITIVE": 1}

    model = AutoModelForSequenceClassification.from_pretrained(
        PARAMS["model_name"], num_labels=2, id2label=id2label, label2id=label2id
    )
    tokenizer = AutoTokenizer.from_pretrained(PARAMS["model_name"])
    data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

    data_files = {
        data_split: str(DATA_DIR.joinpath(f"{data_split}.json"))
        for data_split in ["train", "validation", "test"]
    }
    dataset = load_dataset("json", data_files=data_files, field="data")
    encoded_dataset = dataset.map(
        preprocess_function(tokenizer),
        batched=True,
        remove_columns=["idx", "sentence"],
    )

    if not PARAMS["class_weight"]:
        flat_labels = dataset["train"]["label"]
        class_weights = compute_class_weight(
            class_weight="balanced", classes=np.unique(flat_labels), y=flat_labels
        )
        unique_labels = np.unique(flat_labels)
        weights = {}
        for i in range(len(unique_labels)):
            weights[unique_labels[i]] = class_weights[i]
        PARAMS["class_weight"] = list(class_weights)

    training_args = TrainingArguments(
        output_dir=RES_DIR,
        num_train_epochs=PARAMS["epoch"],
        per_device_train_batch_size=PARAMS["batch_size_train"],
        per_device_eval_batch_size=PARAMS["batch_size_eval"],
        logging_steps=PARAMS["logging_step"],
        metric_for_best_model="f1",
        load_best_model_at_end=True,
        evaluation_strategy="steps",
        save_strategy="steps",
        save_steps=PARAMS["save_step"],
        eval_steps=PARAMS["eval_step"],
        logging_dir=RUN_DIR.joinpath("logs"),
        warmup_ratio=PARAMS["warmup_ratio"],
        weight_decay=PARAMS["weight_decay"],
    )

    trainer = CustomTrainer(
        model=model,
        args=training_args,
        train_dataset=encoded_dataset["train"],
        eval_dataset=encoded_dataset["validation"],
        tokenizer=tokenizer,
        data_collator=data_collator,
        compute_metrics=compute_metrics,
    )

    trainer.train()

    if PARAMS["save_model"]:
        trained_model_path = RUN_DIR.joinpath("ckpt")
        model.save_pretrained(trained_model_path)
        tokenizer.save_pretrained(trained_model_path)

    if PARAMS["clean_results"]:
        clean_dir(RES_DIR)

    result_dict = {"parameters": PARAMS, "evaluation": {}}

    glue_mrpc = evaluate.load("glue", "mrpc")
    recall = evaluate.load("recall")
    precision = evaluate.load("precision")

    if PARAMS["do_eval"]:
        for data in ["train", "validation", "test"]:
            output = trainer.predict(encoded_dataset[data])
            preds = np.argmax(output.predictions, axis=-1)
            metric_dict = {}
            for metric in [glue_mrpc, recall, precision]:
                metric_dict = {
                    **metric_dict,
                    **metric.compute(predictions=preds, references=output.label_ids),
                }
            result_dict["evaluation"][data] = metric_dict

    with open(RUN_DIR.joinpath("run.json"), "w") as f:
        json.dump(result_dict, f)
