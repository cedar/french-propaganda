import json
import os
import random
from pathlib import Path

import evaluate
import numpy as np
import torch
from datasets import load_dataset
from sklearn.metrics import f1_score, precision_score, recall_score
from sklearn.utils.class_weight import compute_class_weight
from torch import nn
from transformers import (
    AutoModelForSequenceClassification,
    AutoTokenizer,
    DataCollatorWithPadding,
    Trainer,
    TrainingArguments,
)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


PARAMS = {
    "model_name": "bert-base-multilingual-cased",
    "learning_rate": 2e-5,
    "batch_size_train": 16,
    "batch_size_eval": 64,
    "save_model": True,
    "epoch": 20,
    "logging_step": 50,
    "save_step": 50,
    "eval_step": 50,
    "seed": 42,
    "warmup_ratio": 0.1,
    "weight_decay": 0.01,
    "clean_results": True,
    "do_eval": True,
    "class_weight": None,
}

BASE_DIR = Path(__file__).parent.parent
DATA_DIR = BASE_DIR.joinpath("data")
RES_DIR = BASE_DIR.joinpath("results")


def set_seed(seed):
    """
    Everything should be reproducible
    :param seed:
    :return:
    """
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    random.seed(seed)
    np.random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    # these are just for deterministic behaviour
    torch.backends.cudnn.benchmark = False
    torch.use_deterministic_algorithms(True)
    os.environ["CUBLAS_WORKSPACE_CONFIG"] = ":4096:8"
    torch.backends.cudnn.deterministic = True


def clean_dir(path):
    for child in path.glob("*"):
        if child.is_file():
            child.unlink()
        else:
            clean_dir(child)
    path.rmdir()


def compute_metrics(eval_preds):
    threshold = 0.5
    metric = evaluate.load("f1", average="macro")
    logits, labels = eval_preds
    sigmoid = torch.nn.Sigmoid()
    probs = sigmoid(torch.Tensor(logits))
    predictions = np.zeros(probs.shape)
    predictions[np.where(probs >= threshold)] = 1
    return {"f1": f1_score(y_true=labels, y_pred=predictions, average="macro")}


class CustomTrainer(Trainer):
    def compute_loss(
        self,
        model,
        inputs,
        return_outputs=False,
    ):
        labels = inputs.get("labels")
        outputs = model(**inputs)
        logits = outputs.get("logits")
        loss_fct = nn.CrossEntropyLoss(
            weight=torch.tensor(PARAMS["class_weight"], dtype=torch.float).to(device)
        )
        loss = loss_fct(
            logits.view(-1, self.model.config.num_labels),
            labels.view(-1, self.model.config.num_labels),
        )
        return (loss, outputs) if return_outputs else loss


if __name__ == "__main__":

    MODEL_DIR = BASE_DIR.joinpath("models", f"{PARAMS['model_name']}-multilabel")
    if not MODEL_DIR.exists():
        MODEL_DIR.mkdir()
    RUN_DIR = MODEL_DIR.joinpath(str(len(list(MODEL_DIR.iterdir()))))

    print("Starting training with PARAMS:\n", PARAMS, "\n")

    set_seed(PARAMS["seed"])

    data_files = {
        data_split: str(DATA_DIR.joinpath(f"{data_split}_multi.json"))
        for data_split in ["train", "validation", "test"]
    }
    dataset = load_dataset("json", data_files=data_files, field="data")

    labels = dataset["train"]["label"]
    flat_labels = [item for sublist in labels for item in sublist]
    class_weights = compute_class_weight(
        class_weight="balanced", classes=np.unique(flat_labels), y=flat_labels
    )
    unique_labels = np.unique(flat_labels)
    weights = {}
    for i in range(len(unique_labels)):
        weights[unique_labels[i]] = class_weights[i]
    PARAMS["class_weight"] = list(class_weights)

    id2label = {i: label for i, label in enumerate(unique_labels)}
    label2id = {label: i for i, label in enumerate(unique_labels)}

    model = AutoModelForSequenceClassification.from_pretrained(
        PARAMS["model_name"],
        num_labels=len(unique_labels),
        id2label=id2label,
        label2id=label2id,
        problem_type="multi_label_classification",
    )
    tokenizer = AutoTokenizer.from_pretrained(PARAMS["model_name"])
    data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

    def preprocess_data(examples):
        text = examples["sentence"]
        encoding = tokenizer(
            text, padding="max_length", truncation=True, max_length=210
        )
        labels_matrix = np.zeros((len(text), len(unique_labels)))
        for idx, label_list in enumerate(examples["label"]):
            for label in label_list:
                label_id = label2id[label]
                labels_matrix[idx, label_id] = 1
        encoding["labels"] = labels_matrix.tolist()
        return encoding

    encoded_dataset = dataset.map(
        preprocess_data, batched=True, remove_columns=dataset["train"].column_names
    )

    training_args = TrainingArguments(
        output_dir=RES_DIR,
        num_train_epochs=PARAMS["epoch"],
        per_device_train_batch_size=PARAMS["batch_size_train"],
        per_device_eval_batch_size=PARAMS["batch_size_eval"],
        logging_steps=PARAMS["logging_step"],
        metric_for_best_model="f1",
        load_best_model_at_end=True,
        evaluation_strategy="steps",
        save_strategy="steps",
        save_steps=PARAMS["save_step"],
        eval_steps=PARAMS["eval_step"],
        logging_dir=RUN_DIR.joinpath("logs"),
        warmup_ratio=PARAMS["warmup_ratio"],
        weight_decay=PARAMS["weight_decay"],
    )

    trainer = CustomTrainer(
        model=model,
        args=training_args,
        train_dataset=encoded_dataset["train"],
        eval_dataset=encoded_dataset["validation"],
        tokenizer=tokenizer,
        data_collator=data_collator,
        compute_metrics=compute_metrics,
    )

    trainer.train()

    if PARAMS["save_model"]:
        trained_model_path = RUN_DIR.joinpath("ckpt")
        model.save_pretrained(trained_model_path)
        tokenizer.save_pretrained(trained_model_path)

    if PARAMS["clean_results"]:
        clean_dir(RES_DIR)

    result_dict = {"parameters": PARAMS, "evaluation": {}}

    if PARAMS["do_eval"]:
        sigmoid = torch.nn.Sigmoid()
        for data in ["train", "validation", "test"]:
            output = trainer.predict(encoded_dataset[data])
            probs = sigmoid(torch.Tensor(output.predictions))
            predictions = np.zeros(probs.shape)
            predictions[np.where(probs >= 0.5)] = 1
            metric_dict = {}
            for metric in [f1_score, precision_score, recall_score]:
                metric_dict[metric.__name__] = metric(
                    y_true=output.label_ids, y_pred=predictions, average="macro"
                )
            result_dict["evaluation"][data] = metric_dict

    with open(RUN_DIR.joinpath("run.json"), "w") as f:
        json.dump(result_dict, f)
